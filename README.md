# django-as2r #

**django-as2r** is an extension to the Django web framework that integrates
adminsortable2 and reversion extensions.


Installation
============

* git clone https://bitbucket.org/hilsein/django-as2r
* cd django-as2r
* python setup.py install

Usage
=====

* Add 'as2r' to INSTALLED_APPS:


```
#!python

  INSTALLED_APPS = [
    ...
    'as2r',
    ...
  ]
```


* Inherit your admin model class from SortableVersionAdminMixin:

your_app/admin.py:

```
#!python

  from as2r.admin import SortableVersionAdminMixin
  
  class SomethingAdmin(SortableVersionAdminMixin):
     pass
  
  admin.site.register(Something, SomethingAdmin)
```
