from __future__ import unicode_literals

from django.contrib import admin

from adminsortable2.admin import SortableAdminMixin
from reversion.admin import VersionAdmin

class SortableVersionAdminMixin(SortableAdminMixin, VersionAdmin):
   change_list_template = 'as2r/change_list.html'

#from reversion_compare.admin import CompareVersionAdmin
#class SortableCompareVersionAdmin(SortableAdminMixin, CompareVersionAdmin):
#   change_list_template = 'as2r/change_list.html'
