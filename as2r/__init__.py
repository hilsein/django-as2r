"""
An extension to the Django web framework that integrates adminsortable and reversion applications

Developed by hilsein@gmail.com

"""

try:
    import django 
except ImportError:  # pragma: no cover
    pass
else:
    pass

__version__ = VERSION = (0, 0, 1)
